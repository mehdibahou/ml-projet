FROM python:3.9.12

# Adding trusting keys to apt for repositories
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -



WORKDIR /app

COPY requirements.txt requirements.txt


RUN pip install --upgrade pip

RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000

CMD [ "python3", "main.py", "--host=0.0.0.0"]